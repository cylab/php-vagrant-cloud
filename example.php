<?php

require_once "./vendor/autoload.php";


// Using a logger to get some feedback...
$logger = new \Monolog\Logger("log");
$logger->pushHandler(new \Monolog\Handler\ErrorLogHandler());

$vagrant = new \Cylab\Vagrant\Vagrant("/tmp/php-vagrant", $logger);
$path = $vagrant->getOVA("olbat/tiny-core-micro");

// /tmp/php-vagrant/olbat/tiny-core-micro/0.1.0/box.ova
echo $path . "\n";

// Or, if we don't need a logger...
$vagrant = new \Cylab\Vagrant\Vagrant();
$path = $vagrant->getOVA("olbat/tiny-core-micro");
echo $path . "\n";
