<?php
namespace Cylab\Vagrant;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Manages vagrant boxes
 *
 * @author tibo
 */
class Vagrant
{

    const BASE_URL = "https://vagrantcloud.com/";

    private $root_directory;

    private $metadata_cache = [];

    /**
     *
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        $root_directory = "/tmp/php-vagrant",
        LoggerInterface $logger = null
    ) {
        $this->root_directory = $root_directory;

        $this->logger = $logger;
        if ($this->logger == null) {
            $this->logger = new NullLogger();
        }

        $this->logger->debug("Vagrant root: " . $this->root_directory);
    }

    /**
     * Download (if needed) the latest or given version of this box
     * (e.g. ubuntu/bionic64), and return the path to the local ova image.
     * @param string $box_name
     */
    public function getOVA($box_name, $version = "latest") : string
    {
        if ($version == "latest") {
            $box = $this->getLatestBoxVersion($box_name);
        } else {
            $box = $this->getBoxVersion($box_name, $version);
        }

        $local_path = $this->getLocalOVAPath($box);
        if (!file_exists($local_path)) {
            $this->downloadAndRepack($box);
        }

        return $local_path;
    }

    /**
     * Get the metadata for this box (json encoded).
     * @param string $box
     * @return string
     */
    public function getMetadata($box) : object
    {

        if (isset($this->metadata_cache[$box])) {
            return $this->metadata_cache[$box];
        }

        $url = self::BASE_URL . $box;
        $this->logger->notice("Downloading metadata from $url");

        $json_metadata = $this->download($url);
        if ($json_metadata === false) {
            throw new \Exception("Failed to download metadata for $box");
        }
        $metadata = json_decode($json_metadata);

        if ($metadata == null) {
            throw new \Exception("Failed to find box $box");
        }
        
        if (isset($metadata->success) && $metadata->success === false) {
            throw new \Exception("Failed to find box $box");
        }

        $this->metadata_cache[$box] = $metadata;

        return $metadata;
    }

    /**
     * Get the BoxVersion descriptor for a given version of a box.
     * @param string $box_name
     * @param string $version_tag
     * @return \Cylab\Vagrant\BoxVersion
     * @throws \Exception
     */
    public function getBoxVersion(string $box_name, string $version_tag)
            : BoxVersion
    {
        $metadata = $this->getMetadata($box_name);

        $box_version = new BoxVersion($box_name);
        foreach ($metadata->versions as $version) {
            if ($version->version == $version_tag) {
                $box_version->parseMetadata($version);
                return $box_version;
            }
        }

        throw new \Exception("Version $version_tag of box $box_name not found");
    }

    /**
     *
     * @param string box (e.g. cylab/ubuntu-server-16.04)
     * @return \Cylab\Cyrange\Vagrant\BoxVersion
     */
    public function getLatestBoxVersion(string $box) : BoxVersion
    {
        $metadata = $this->getMetadata($box);

        $box_version = new BoxVersion($box);
        $box_version->parseMetadata($metadata->versions[0]);

        $this->logger->notice("Latest version is " . $box_version->version);
        return $box_version;
    }

    public function getLocalOVAPath(BoxVersion $latest) : string
    {
        return $this->root_directory . "/" . $latest->box . "/" . $latest->version
                . "/box.ova";
    }

    public function downloadAndRepack(BoxVersion $latest)
    {
        $local_ova_path = $this->getLocalOVAPath($latest);
        $dirname = dirname($local_ova_path);
        if (!is_dir($dirname)) {
            mkdir($dirname, 0777, true);
        }

        $local_box_path = $dirname . "/virtualbox.box";
        $this->logger->notice("Downloading from " . $latest->url);
        $this->downloadToFile($latest->url, $local_box_path);
        $this->repack($local_box_path, $local_ova_path);
    }

    public function download($url)
    {
        $ch = curl_init($url);

        $proxy = getenv("http_proxy");
        if ($proxy !== null && $proxy !== "") {
            $proxy = trim(str_replace("http://", "", $proxy), '/');
            curl_setopt($ch, CURLOPT_PROXY, $proxy);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    public function downloadToFile($url, $local_path)
    {
        $ch = curl_init($url);
        $fp = fopen($local_path, "w");

        $proxy = getenv("http_proxy");
        if ($proxy !== null && $proxy !== "") {
            $proxy = trim(str_replace("http://", "", $proxy), '/');
            curl_setopt($ch, CURLOPT_PROXY, $proxy);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_NOPROGRESS, false);
        curl_setopt($ch, CURLOPT_PROGRESSFUNCTION, [$this, 'progress']);

        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
    }

    public function progress(
        $resource,
        $download_size,
        $downloaded,
        $upload_size,
        $uploaded
    ) {

        static $previous_progress = 0;
        static $start_time = 0;

        if ($download_size == 0) {
            return;
        }

        if ($start_time == 0) {
            $start_time = time();
        }

        $progress = round($downloaded * 100 / $download_size);

        if ($progress == 100) {
            return;
        }

        if ($progress <= $previous_progress) {
            return;
        }

        $previous_progress = $progress;
        $elapsed_time = time() - $start_time;
        $remaining = round($elapsed_time / $progress * (100 - $progress));

        $this->logger->notice("$progress% ($remaining sec remaining)");
    }

    public function repack($local_box_path, $local_ova_path)
    {
        $this->logger->notice("Repacking $local_ova_path");
        $dir = dirname($local_box_path);

        $this->execute("tar xf $local_box_path -C $dir");
        $this->execute("cd $dir && tar cf $local_ova_path *.ovf *.vmdk");
        $this->execute("cd $dir && rm $local_box_path *.ovf *.vmdk");
    }

    public function execute(string $cmd)
    {
        $process = new Process($cmd);
        $process->run();

        if (!$process->isSuccessful()) {
            if (! str_contains(
                $process->getErrorOutput(),
                "tar: Ignoring unknown extended header keyword 'SCHILY.fflags'"
            )) {
                throw new ProcessFailedException($process);
            }
        }

        $out = $process->getOutput();
        if ($out !== "") {
            $this->logger->debug($out);
        }

        $err = $process->getErrorOutput();
        if ($err !== "") {
            $this->logger->warning($err);
        }
    }
}

function str_contains(string $haystack, string $needle)
{
    return strpos($haystack, $needle) !== false;
}
