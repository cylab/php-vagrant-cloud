<?php

namespace Cylab\Vagrant;

/**
 * Description of BoxVersion
 *
 * @author tibo
 */
class BoxVersion
{
    public $box;
    public $version;
    public $url;

    public function __construct($box)
    {
        $this->box = $box;
    }

    public function parseMetadata($metadata)
    {
        $this->version = $metadata->version;

        foreach ($metadata->providers as $provider) {
            if ($provider->name == "virtualbox") {
                $this->url = $provider->url;
                return;
            }
        }
    }
}
