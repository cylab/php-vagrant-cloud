# php-vagrant-cloud

[![pipeline status](https://gitlab.cylab.be/cylab/php-vagrant-cloud/badges/master/pipeline.svg)](https://gitlab.cylab.be/cylab/php-vagrant-cloud/commits/master)
[![coverage report](https://gitlab.cylab.be/cylab/php-vagrant-cloud/badges/master/coverage.svg)](https://gitlab.cylab.be/cylab/php-vagrant-cloud/commits/master)

A PHP client for Vagrant Cloud. Allows to download vagrant boxes.

## Installation

```bash
composer require cylab-be/php-vagrant-cloud
```

## Usage

```php
<?php

require_once "./vendor/autoload.php";

$vagrant = new \Cylab\Vagrant\Vagrant();
$path = $vagrant->getOVA("olbat/tiny-core-micro");

// /tmp/php-vagrant/olbat/tiny-core-micro/0.1.0/box.ova
echo $path;
```

Using a logger (works with any implementation of PSR Logger):

```php
$logger = new \Monolog\Logger("log");
$logger->pushHandler(new \Monolog\Handler\ErrorLogHandler());

$vagrant = new \Cylab\Vagrant\Vagrant("/tmp/php-vagrant", $logger);
$path = $vagrant->getOVA("olbat/tiny-core-micro");

// /tmp/php-vagrant/olbat/tiny-core-micro/0.1.0/box.ova
echo $path;
```

