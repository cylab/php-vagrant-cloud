<?php

namespace Cylab\Vagrant;

class VagrantTest extends \PHPUnit\Framework\TestCase
{


    protected function setUp() : void
    {
        exec("rm -Rf " . $this->getRoot());
    }

    /**
     * Get the directory for storing the downloaded boxes.
     * @return string
     */
    public function getRoot() : string
    {
        return sys_get_temp_dir() . "/vagrant-test/";
    }

    public function testGetMetadata()
    {
        $basedir = $this->getRoot();
        $result = json_decode(file_get_contents(__DIR__ . "/cylab_basebox_metadata.txt"));
        $box = "cylab/ubuntu-16.04-64-server-base";
        $vagrant = new Vagrant($basedir);
        $this->assertEquals($result, $vagrant->getMetadata($box));
    }

    public function testGetLatestBoxVersion()
    {
        $basedir = $this->getRoot();
        $vagrant = new Vagrant($basedir);
        $box_version = $vagrant->getLatestBoxVersion("cylab/ubuntu-16.04-64-server-base");
        $this->assertEquals("20181226.175800.0", $box_version->version);
    }

    public function testDownloadToFile()
    {
        $basedir = $this->getRoot();
        $vagrant = new Vagrant($basedir);
        $url = "https://vagrantcloud.com/cylab/ubuntu-16.04-64-server-base";
        $local_path = \tempnam(sys_get_temp_dir(), "vagtest");
        $vagrant->downloadToFile($url, $local_path);
        $metadata = file_get_contents(__DIR__ . "/cylab_basebox_metadata.txt");
        $this->assertEquals($metadata, file_get_contents($local_path));
    }

    public function testGetOVA()
    {
        // this box is only 12MB large, but old...
        $box = "olbat/tiny-core-micro";

        $basedir = $this->getRoot();
        $vagrant = new Vagrant($basedir);
        $path = $vagrant->getOVA($box);
        $this->assertTrue(is_file($path));
    }

    public function testTinyCore()
    {
        // this box is 20MB large, and packed with a recent version of vagrant
        $box = "cylab/tiny-core";
        $basedir = $this->getRoot();
        $vagrant = new Vagrant($basedir);
        $path = $vagrant->getOVA($box);
        $this->assertTrue(is_file($path));
    }


    public function testUbuntuServer()
    {
        // this is one of our large boxes (1.9GB)
        $box = "cylab/ubuntu-16.04-64-server";
        $version = "20190704.231142.0";
        $basedir = $this->getRoot();
        $vagrant = new Vagrant($basedir);
        $path = $vagrant->getOVA($box, $version);
        $this->assertTrue(is_file($path));
    }

    /**
     * @group not-found-exception
     */
    public function testBoxNotFoundException()
    {
        $this->expectException(\Exception::class);
        $box = "qmsldhj";
        $basedir = $this->getRoot();
        $vagrant = new Vagrant($basedir);
        $path = $vagrant->getOVA($box);
    }

    /**
     * @group ova-version
     */
    public function testGetOVAVersion()
    {
        $basedir = $this->getRoot();
        $vagrant = new Vagrant($basedir);
        // this box is only 12MB large...
        $path = $vagrant->getOVA("olbat/tiny-core-micro", "0.1.0");
        $this->assertTrue(is_file($path));
    }
}
